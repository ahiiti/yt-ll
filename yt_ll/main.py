#!/usr/bin/env python3

import os
import argparse
import subprocess
import csv

ytdl_commands = ['yt-dlp', 'youtube-dl']

parser = argparse.ArgumentParser(usage='yt-ll [OPTIONS] LIST_FILE [ -- YOUTUBEDL_ARGUMENTS ]')


parser.add_argument('--ytdl', help='optional name/path of Youtube-DL command', default=None)
parser.add_argument('--seperator', help='seperator of name and url in list file', default=';')
parser.add_argument('--quoting', help='names and urls in list are quoted (for csv files)', action='store_true')
parser.add_argument('--name-col', help='column of output file name (for csv files) (counting from 0)', default=0)
parser.add_argument('--url-col', help='column of download URL (for csv files) (counting from 0)', default=1)

parser.add_argument('list_file', type=str, metavar='LIST_FILE')

parser.add_argument('ytdl_args', nargs='*', default=[], metavar='YOUTUBEDL_ARGUMENTS')

args = parser.parse_args()


list_path = args.list_file
ytdl_command = args.ytdl
ytdl_args = args.ytdl_args
list_seperator = args.seperator
url_col = args.url_col
name_col = args.name_col

csv_quoting = csv.QUOTE_NONE
if args.quoting:
  csv_quoting = csv.QUOTE_ALL

def command_exists(cmd):
  result = subprocess.run(['which', cmd], capture_output=True)
  return (result.returncode == 0)

def main():
  global ytdl_command
  if ytdl_command:
    if not command_exists(ytdl_command):
      print('Custom Youtube-DL command not found!')
      exit()
  else:
    for cmd in ytdl_commands:
      if command_exists(cmd):
        ytdl_command = cmd
        break

  if not ytdl_command:
    print('Youtube-DL seems to be not installed or not in your PATH. Exiting.')
    exit()

  print(f'List path: {list_path}')
  print(f'Youtube-DL command: {ytdl_command}')


  def ytdl(url, name=None):
    pass
    #subprocess.run(['


  if (not os.path.exists(list_path)) or (not os.path.isfile(list_path)):
    print('List file doesn\'t exist. Exiting.')
    exit()

  with open(list_path, newline='') as list_file:
    reader = csv.reader(list_file, delimiter=list_seperator, quoting=csv_quoting)
    for row in reader:
      row = [col.strip() for col in row]
      name = row[name_col]
      url = row[url_col]
      subprocess.run([ytdl_command, '-o', f'{name}.%(ext)s'] + ytdl_args + [url])
