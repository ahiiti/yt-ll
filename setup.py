#!/usr/bin/env python3

from setuptools import setup, Command


setup(name = 'yt-ll',
    version = '0.1',
    description = 'youtube-dl link list downloader with custom file names.',
    platforms = ['linux'],
    author = 'ahiiti',
    license = 'GPL3',
    packages = ['yt_ll'],
    entry_points = {
        'console_scripts': ['yt-ll = yt_ll.main:main']
    })