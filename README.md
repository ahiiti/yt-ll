Youtube-DL list loader
======================

A quick and dirty tool to download a list of videos with custom output file names. This can be useful when downloading lots of DASH or M3U manifests without human-readable file names.

Installation
------------

    git clone https://gitlab.com/ahiiti/yt-ll
    cd yt-ll
    pip3 install .

Usage
-----

This will download everything from a list:

    yt-ll list.txt

The default list format is a filename and url seperated by a semicolon:

    filename1; https://url-to-video1
    filename2; https://url-to-video2
    [...]

Custom list formats
-------------------

`yt-ll` can also read custom list formats.

The column seperator character can be changed using the `--seperator` option.
The columns of the file name and url can be set using the `--name-col` and `--url-col` option. Column numbers start with zero.
If the values of columns are in quotation marks, use the `--quoting` option.

So, if we would want to download from a CSV file like this:

    "some notes", "https://url-to-video-1", "filename1", "other notes"
    "foo",        "https://url-to-video-2", "filename2", "bar"

It can be downloaded using:

    yt-ll --quoting --name-col 2 --url-col 1 --seperator ',' list.csv

Passing arguments to Youtube-DL
-------------------------------

All command line options following `--` will be passed to Youtube-DL:

    yt-ll list.txt -- --extract-audio --audio-format opus --download-archive downloaded.txt

This will pass `--extract-audio --audio-format opus --download-archive downloaded.txt` to Youtube-DL.

Youtube-DL executable
---------------------

`yt-ll` will auto-detect `youtube-dl` and `yt-dlp` (if installed, `yt-dlp` will be preferred).
It doesn't matter if you installed Youtube-DL via pip or the sytem package manager. `yt-ll` uses whatever is available as a command in the terminal.

Use `which youtube-dl` or `which yt-dlp` to see which executable is used.

If `youtube-dl` is not in your PATH or you want to use another version, use the `--ytdl` option:

    yt-ll --ytdl '/usr/bin/youtube-dl' list.txt

Downloading playlists
---------------------

`yt-ll` was mainly made for batch-downloading single videos.
If one of the download URLs in the list is a playlist, there is a problem: All videos of the playlist would be downloaded to the same file name (the value in the file name column).

The value of the name column is passed to the output template argument of Youtube-DL. This means, you can also put placeholders in the name value.

If you choose `%(playlist_index)s playlist` as the file name for a playlist, the downloaded files will be `1 playlist.mp4`, `2 playlist.mp4` and so on.